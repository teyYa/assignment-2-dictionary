%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%define MAX_WORD_SIZE 255
%define WORD 8
%define ERROR 1
%define ERROR_NOT_FOUND 2
%define BUFFER_SIZE 255
extern find_word

section .rodata
key_not_found_message:
	db "Данный ключ не найден", 0
overflow_message:
	db "Длина строки должна быть менее 255 символов", 0

section .bss
buffer: resb BUFFER_SIZE

section .text
global _start
_start:
	mov rdi, buffer
	mov rsi, MAX_WORD_SIZE
	call read_string
	test rax, rax
	je .buffer_overflow
	
	mov rdi, rax
	mov rsi, next_element
	call find_word
	test rax, rax
	je .not_found

	add rax, POINTER
	mov rdi, rax
	push rdi
	call string_length
	inc rax
	pop rdi
	add rdi, rax
	call print_string
	call print_newline
	mov rax, 0
	mov rdi, 0
	jmp exit

	.buffer_overflow:
		mov rdi, overflow_message
		call print_string_to_stderr
		mov rdi, ERROR
		jmp exit
	.not_found:
		mov rdi, key_not_found_message
		call print_string_to_stderr
		mov rdi, ERROR_NOT_FOUND
		call exit
