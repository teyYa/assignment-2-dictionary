%include "lib.inc"
%define POINTER 16
section .text
global find_word
find_word:
	test rsi, rsi
		jz ._end
		mov r8, rsi
		mov r9, rdi

		._loop:
			add rsi, POINTER
			
			call string_equals

			test rax, rax
			jnz ._found
			mov rsi, qword [r8]
			test rsi, rsi
			jz ._end
			mov r8, rsi
			mov rdi, r9
			jmp ._loop
		._found:
			mov rax, r8
		ret
		._end:
			xor rax, rax
		ret


