%define next_element 0

%macro colon 2
    %ifnarg 2
        %error "Ошибка, ожидалось два аргумента"
    %ifidni %1, dq
        %error "Некорректный первый аргумент"
    %endif
    
    %ifidni %2, db
        %error "второй аргумент должен быть меткой"
    %endif

    %2:
    dq next_element
    db %1, 0
    %define next_element %2
%endmacro
